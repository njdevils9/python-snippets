#!/usr/bin/python
# Author: Jon Murillo

## Adding sample code to setup some basic logging. Will need to be tweaked if you are doing something more complex.

import logging

def anotherFunction():
   print('Entered anotherFunction')
   logger.info('logging a message no matter if debug mode is on/off')
   logger.info('status of debug mode: {}'.format(debug_mode)
)

def main():
   print('Entered main function')
   
   if debug_mode == 'true':
      logger.info('logging debug message since debug_mode is turned on.')

   anotherFunction()

if __name__ == '__main__':

    logger = logging.getLogger()
    logger.setLevel(logging.INFO)

    ## create console handler
    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)

    ## create log message formatter
    formatter = logging.Formatter('%(asctime)s %(levelname)s: %(message)s')

    ## format our console handler
    ch.setFormatter(formatter)

    logger.addHandler(ch)

    ## toggle debug mode
    debug_mode = 'true'

    main()

